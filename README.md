# 🌏 geo-bi

---

# Dev Notes

## Development Prerequisites

### Power BI SDK Installation

Follow this [tutorial](https://docs.microsoft.com/en-us/power-bi/developer/visuals/environment-setup?tabs=windows) from the Power BI custom visual documentation. For npm global install access issues, consult this StackOverflow [thread](https://stackoverflow.com/questions/48910876/error-eacces-permission-denied-access-usr-local-lib-node-modules).

After installation, start a new project:
```
pbiviz new myNewProject
```

Then run the visual server by running 

```
pbiviz start
```

For this project, also install a utility library for handling tooltips

```
npm install powerbi-visuals-utils-tooltiputils --save
```

Notes:

- For windows, if pbiviz is successfully installed but cannot be run in CLI, manually add pbiviz.cmd location to system path in environment variables.
- If visual server successfully runs but Power BI service indicates that it can't connect to visual server, go to https://localhost:8080/assets/status, skip the certificate warning, and open the page. Then check back with the power BI debug visual. [(Reference)](https://docs.microsoft.com/en-us/power-bi/developer/visuals/develop-circle-card#view-the-circle-card-in-power-bi-service)

### Leaflet Installation

1) Install leaflet and typings

    ```
    npm install --save leaflet @types/leaflet
    ```

1) Add leaflet css. Try adding this line to `style/visual.less`.

    ```
    @import (less) "node_modules/leaflet/dist/leaflet.css";
    ```

    According to [this](http://avinmathew.com/using-leaflet-in-a-power-bi-custom-visual/) it should work. However I encountered compilation issues, and what worked was importing the css file in `src/visual.ts`.

    ```
    import "leaflet/dist/leaflet.css"
    ```

1) Leaflet interactivity with D3

### Other libraries

D3, wellknown, and osmtogeojson are alse used in this visual. D3 is installed by default, soinstall the two modules and type definitions via npm.

    npm install --save wellknown @types/wellknown osmtogeojson @types/osmtogeojson

## Resources

1) Power BI Custom Visuals Contest Repos
1) Power BI Sample Charts Repo
    - https://github.com/microsoft/powerbi-visuals-drilldown-cartogram/blob/master/src/visual.ts
    - https://github.com/microsoft/PowerBI-visuals-sampleBarChart/blob/master/Tutorial/ReportPageTooltips.md
1) Bar chart walkthrough
    - https://www.youtube.com/watch?v=sVGc6sLOoYM&list=PL6z9i4iVbl8C2mtjFlH3ECb3q00eFDLAG&index=13
1) D3 and Leaflet Demo
    - https://observablehq.com/@sfu-iat355/intro-to-leaflet-d3-interactivity
1) D3 Scales
    - https://observablehq.com/@d3/quantile-quantize-and-threshold-scales
    - https://gist.github.com/michellechandra/0b2ce4923dc9b5809922
    - https://medium.com/@anilnairxyz/choropleth-map-using-d3-a8cfa9c5e209
    - https://observablehq.com/@richdayandnight/one-chart-a-day-summarizing-how-to-make-a-map-with-d3-and-geojs
1) Power BI Legend Service, Color Utils, and Data View Utils
    - https://docs.microsoft.com/en-us/power-bi/developer/visuals/utils-chart#legend-service
    - https://docs.microsoft.com/en-us/power-bi/developer/visuals/utils-color
    - https://docs.microsoft.com/en-us/power-bi/developer/visuals/utils-dataview
1) Objects
    - https://docs.microsoft.com/en-us/power-bi/developer/visuals/capabilities#define-property-pane-options-objects
    - https://docs.microsoft.com/en-us/power-bi/developer/visuals/objects-properties
1) Data Binding
    - https://community.powerbi.com/t5/Developer/Custom-Visualization-additional-DataViewCategoryColumn/td-p/476360