"use strict";

import "core-js/stable";
import "./../style/visual.less";
import powerbi from "powerbi-visuals-api";
import VisualConstructorOptions = powerbi.extensibility.visual.VisualConstructorOptions;
import VisualUpdateOptions = powerbi.extensibility.visual.VisualUpdateOptions;
import IVisual = powerbi.extensibility.visual.IVisual;
import EnumerateVisualObjectInstancesOptions = powerbi.EnumerateVisualObjectInstancesOptions;
import VisualObjectInstance = powerbi.VisualObjectInstance;
import DataView = powerbi.DataView;
import VisualObjectInstanceEnumerationObject = powerbi.VisualObjectInstanceEnumerationObject;

// Additional Imports
import * as d3 from "d3"; 
type Selection<T extends d3.BaseType> = d3.Selection<T, any, any, any>; // from docs
import IVisualHost = powerbi.extensibility.visual.IVisualHost;
import * as L from "leaflet";
import "leaflet/dist/leaflet.css"
import ISelectionManager = powerbi.extensibility.ISelectionManager;
import ISelectionId = powerbi.visuals.ISelectionId;
import VisualTooltipDataItem = powerbi.extensibility.VisualTooltipDataItem;
import TooltipShowOptions = powerbi.extensibility.TooltipShowOptions;
import { createTooltipServiceWrapper, ITooltipServiceWrapper, TooltipEventArgs } from "powerbi-visuals-utils-tooltiputils";
import * as osmtogeojson from "osmtogeojson";
import * as wellknown from "wellknown";

// Interfaces
interface DataPoint {
    category: string;
    lat?: number;
    lng?: number;
    latlng?: {};
    selectionID: ISelectionId;
    wkt?: string;
    tooltipFields?: [];
    tooltipItems: VisualTooltipDataItem[];
    highlighted: boolean;
    legend?: string;
    color: string;
    pointScale?: number;
    wktScale?: number;
}
interface ViewModel {
    dataPoints: DataPoint[],
    latMax: number; // maxes and mins for setting default map view
    latMin: number;
    lngMax: number;
    lngMin: number;
    pointScaleMin?: number;
    pointScaleMax?: number;
    wktScaleMin?: number;
    wktScaleMax?: number;
    highlights: boolean;
}

import { VisualSettings } from "./settings";
import { dataRoleHelper } from "powerbi-visuals-utils-dataviewutils";
export class Visual implements IVisual {
    private settings: VisualSettings;
    private host: IVisualHost;
    private target: HTMLElement;
    private selectionManager: ISelectionManager;
    private tooltipServiceWrapper: ITooltipServiceWrapper;

    // containers
    private mapDiv: HTMLElement;
    private map: L.Map;
    private svg: Selection<SVGElement>;
    private baseLayers: {};
    private oldBaseLayer: any;
    private layersControl: any;
    private circleGroup: Selection<SVGElement>;

    // map defaults
    private latInitial: number;
    private lngInitial: number;
    private zoomInitial: number;

    constructor(options: VisualConstructorOptions) {
        // TODO: Add reference for LeafletxPBI in Docs 
        this.target = options.element;
        this.host = options.host;
        this.selectionManager = this.host.createSelectionManager();
        this.tooltipServiceWrapper = createTooltipServiceWrapper(
            this.host.tooltipService,
            options.element
        );
        this.mapDiv = document.createElement('div');
        this.mapDiv.id = 'map';
        this.mapDiv.style.height = '100%';
        this.mapDiv.style.width = '100%';
        this.target.appendChild(this.mapDiv);

        this.map = L.map('map');
        this.latInitial = 14;
        this.lngInitial = 121;
        this.zoomInitial = 8;
        this.map.setView([this.latInitial,this.lngInitial],this.zoomInitial);

        //Creation of map tiles
        var OSM = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> Contributors',
            maxZoom: 18,
            });

        var CartoDB_Positron = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
            maxZoom: 19
        });
        var CartoDB_DarkMatter = L.tileLayer('https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}{r}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
            maxZoom: 19
        });
        var Stamen_TonerLite = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}{r}.png', {
            attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            minZoom: 0,
            maxZoom: 20
        });
        var Stamen_Toner = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner/{z}/{x}/{y}{r}.png', {
            attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            minZoom: 0,
            maxZoom: 20,
        });
        var Stamen_Terrain = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}{r}.png', {
            attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            minZoom: 0,
            maxZoom: 18
        });

		//Base layers definition and addition
		this.baseLayers = {
            "OpenStreetMap": OSM,
			"CartoDB Positron": CartoDB_Positron,
            "CartoDB DarkMatter": CartoDB_DarkMatter,
            "Stamen Toner": Stamen_Toner,
            "Stamen TonerLite": Stamen_TonerLite,
            "Stamen Terrain": Stamen_Terrain
		};

        // Add baseLayers to map as control layers
        this.layersControl = L.control.layers(this.baseLayers).addTo(this.map);

        /* Add d3 element to overlay pane
    
        Original line which was from http://bl.ocks.org/d3noob/9211665

        this.svg = d3.select(this.map.getPanes().overlayPane)
            .append('svg')

        Causes clipping of svg elements in one tile. Changed according to 
        https://github.com/Leaflet/Leaflet/issues/2814#issuecomment-279592011
        */
        L.svg().addTo(this.map);
        d3.select('#map').style('background', 'white');
        this.svg = <Selection<SVGElement>>d3.select("#map").select('svg').attr("pointer-events", "auto");
    }

    public update(options: VisualUpdateOptions) {
        console.log('Visual update');
        
        // Remove svg group workaround
        d3.selectAll('g').remove()
        
        //update map size
        this.mapDiv.style.height = options.viewport.height.toString() + "px";
        this.mapDiv.style.width = options.viewport.width.toString() + "px";
        
        this.settings = Visual.parseSettings(options.dataViews[0]);
        let selectionManager = this.selectionManager;
        let settings = this.settings;
        let viewModel = this.getViewModel(options); // Get data
        console.log(viewModel.dataPoints)
        const map = this.map // for scoping on this and following functions
        const projection = d3.geoTransform({point: function (x, y) {
            const point = map.latLngToLayerPoint(new L.LatLng(y,x))
            this.stream.point(point.x, point.y)
        }});
        const pathCreator = d3.geoPath().projection(projection);
        // map.eachLayer( (layer: any) =>{ 
        //     if (layer instanceof L.TileLayer) {
        //         map.removeLayer(layer);
        //         console.log('layer removed', layer.options.pane);
        //     }
        // });
        if (this.oldBaseLayer && this.oldBaseLayer !== this.baseLayers[this.settings.baseMap.layer]) { 
            console.log('old and current not same. removing old layer...')
            map.removeLayer( this.oldBaseLayer ) 
        };
        if (this.settings.baseMap.layer ) {
            map.addLayer( this.baseLayers[this.settings.baseMap.layer]);
            console.log('current layer', this.settings.baseMap.layer );
            this.oldBaseLayer = this.baseLayers[this.settings.baseMap.layer];
        };
        if (viewModel.latMax && viewModel.lngMax) {
            map.fitBounds([
                [viewModel.latMax, viewModel.lngMax],
                [viewModel.latMax, viewModel.lngMin],
                [viewModel.latMin, viewModel.lngMax],
                [viewModel.latMin, viewModel.lngMin],
            ]);
        };
        let hasWktScale = (<Object>viewModel).hasOwnProperty('wktScaleMax');
        let hasWkt = (<Object>viewModel.dataPoints[0]).hasOwnProperty('wkt');
        if ( hasWkt ) {
            let wktGroup = this.svg.append('g'); // add wkt layer
            let samplewkt = viewModel.dataPoints[0].wkt;
            let samplegeo =  wellknown.parse(samplewkt) ;
            // console.log(samplegeo);
            let wktScaleColor;
            if (hasWktScale) {
                let range = [this.settings.dataPoint.wktFillGradientStart,
                    this.settings.dataPoint.wktFillGradientEnd];
                if (this.settings.dataPoint.wktFillGradientMid 
                    && this.settings.dataPoint.wktSwitchFillGradientMid) {
                        range = [
                            this.settings.dataPoint.wktFillGradientStart,
                            this.settings.dataPoint.wktFillGradientMid,
                            this.settings.dataPoint.wktFillGradientEnd
                        ]
                };
                wktScaleColor = d3.scaleLinear()
                    .domain([viewModel.wktScaleMin, viewModel.wktScaleMax])
                    // @ts-ignore
                    .range(range);
            }
            
            let wkts = viewModel.dataPoints.map(d => wellknown.parse(d.wkt) );
            wkts = wkts.filter(d => d != null);

            const paths: any = wktGroup.selectAll('path')
                .data(wkts)
                .join('path')
                .classed('path', true)
                .attr('fill', (d, i) => {
                    if (hasWktScale) {
                        // console.log(i);
                        console.log('color:', wktScaleColor(viewModel.dataPoints[i].wktScale));
                        return wktScaleColor(viewModel.dataPoints[i].wktScale);
                    } else {
                        return this.settings.dataPoint.wktFill ? this.settings.dataPoint.wktFill : viewModel.dataPoints[i].color;
                    }
                })
                .attr('fill-opacity', (d, i) => viewModel.highlights ? viewModel.dataPoints[i].highlighted ? 1.0 : 0.3 : this.settings.dataPoint.wktFillOpacity)
                .attr('stroke', this.settings.dataPoint.wktStrokeColor)
                .attr('stroke-opacity', this.settings.dataPoint.wktStrokeOpacity)
                .attr('stroke-width', this.settings.dataPoint.wktStrokeThickness)
                // @ts-ignore
                .on("mouseover", (d, i: number) => {
                    // @ts-ignore
                    let mouse = d3.mouse(this.svg.node());
                    let x = mouse[0], y = mouse[1];
                    console.log('mouseover', x, y, i);
                    this.host.tooltipService.show({
                        dataItems: viewModel.dataPoints[i].tooltipItems,
                        identities: [viewModel.dataPoints[i].selectionID],
                        coordinates: [x,y],
                        isTouchEvent: false
                    })
                })
                // @ts-ignore
                .on("click", function (d, i: number) {
                    selectionManager
                        .select(viewModel.dataPoints[i].selectionID)
                        .then((ids: ISelectionId[]) => {
                            paths.style('fill-opacity', ids.length > 0 ? 0.3 : settings.dataPoint.wktFillOpacity)
                                .style('stroke-opacity', ids.length > 0 ? 0.5 : settings.dataPoint.wktStrokeOpacity);
                            d3.select(this).style('fill-opacity', ids.length > 0 ? 1 : settings.dataPoint.wktFillOpacity);
                        }) 
                })
                .style('pointer-events', 'auto')
            // Function to place svg based on zoom
            const onZoom = () => paths.attr('d', pathCreator) //

            // Tooltip Service
            // console.log('adding tooltips')
            this.tooltipServiceWrapper.addTooltip(wktGroup.selectAll('.path'),
                (tooltipEvent: TooltipEventArgs<DataPoint>) => this.getTooltipData(tooltipEvent.data),
                (tooltipEvent: TooltipEventArgs<DataPoint>) => tooltipEvent.data.selectionID
            );

            // initialize positioning
            onZoom()
            // reset whenever map is moved
            map.on('zoomend', onZoom)
            // Workaround is to feed the geoJSON to leaflet, skip null geometries,
            // and call getBounds. Search for correct usage of d3.geoBounds
            console.log( wkts );
            map.fitBounds( L.geoJSON({ "type" : "FeatureCollection", "features": wkts } as any  ).getBounds()
            )
        }

        let hasPointScale = (<Object>viewModel).hasOwnProperty('pointScaleMax');
        let hasLatAndLng = (<Object>viewModel.dataPoints[0]).hasOwnProperty('lat') && (<Object>viewModel.dataPoints[0]).hasOwnProperty('lng');
        if (hasLatAndLng) {
            // add circles/points
            let scaleFactor = this.settings.dataPoint.pointSizeScaling;
            let pointScaleSize;
            if (hasPointScale) {
                pointScaleSize = d3.scaleLinear()
                    .domain([viewModel.pointScaleMin, viewModel.pointScaleMax])
                    .range([5 * scaleFactor, 70 * scaleFactor]);
            }
            let circleGroup = this.svg.append('g'); // Workaround for this.circleGroup not removing elements on exit
            var circles = circleGroup.selectAll('.circle')
                .data(viewModel.dataPoints)
                .enter().append('circle').classed('circle', true)
                // .attr('cx', function (d) { return map.latLngToLayerPoint(d.latlng as any).x })
                // .attr('cy', function (d) { return map.latLngToLayerPoint(d.latlng as any).y })
                .style('stroke', d => this.settings.dataPoint.pointStrokeColor ? this.settings.dataPoint.pointStrokeColor : d.color)
                .style('stroke-width', this.settings.dataPoint.pointStrokeThickness)
                .style('fill',  d => this.settings.dataPoint.pointFill ? this.settings.dataPoint.pointFill : d.color)
                .attr('r', function(d) {
                    if (hasPointScale) {
                        return pointScaleSize(d.pointScale);
                    } else {
                        return 7 * scaleFactor
                    }
                })
                .style('fill-opacity', d => viewModel.highlights ? d.highlighted ? 1.0 : 0.3 : this.settings.dataPoint.pointFillOpacity)
                .style('stroke-opacity', d => viewModel.highlights ? d.highlighted ? 1.0 : 0.3 : this.settings.dataPoint.pointStrokeOpacity);
            // circles
            //     .on('mouseover', function() { //function to add mouseover event
            //         d3.select(this).transition() //D3 selects the object we have moused over in order to perform operations on it
            //             .duration(150) //how long we are transitioning between the two states (works like keyframes)
            //             // .style("fill", "red") //change the fill
            //             .attr('r', 15) //change radius
            //         })
            //     .on('mouseout', function() { //reverse the action based on when we mouse off the the circle
            //         d3.select(this).transition()
            //             .duration(150)
            //             // .style("fill", "blue")
            //             .attr('r', 7)
            //         });
            // Allow highlighting from other charts

            
            // Fade inactive points when selecting
            let callOsmPois = this.callOsmPois;
            let svg = this.svg;
            let host = this.host;
            circles.on('click', function (d) {
                selectionManager
                    .select(d.selectionID)
                    .then((ids: ISelectionId[]) => {
                        // set all not selected to transparent
                        circles.style('fill-opacity', ids.length > 0 ? 0.3 : 0.7)
                            .style('stroke-opacity', ids.length > 0 ? 0.5 : 1.0);
                        d3.select(this).style('fill-opacity', ids.length > 0 ? 1 : settings.dataPoint.pointFillOpacity);
                    });
                let pois = callOsmPois(d.lat, d.lng, host, map, svg, pathCreator);
                console.log('osmmm', osmtogeojson(pois));

            })
    

            // set positions of circles per update
            function update() {
                // circles.attr('transform',
                //     function (d) {
                //         return "translate(" +
                //             map.latLngToLayerPoint(d.latlng as any).x + "," +
                //             map.latLngToLayerPoint(d.latlng as any).y + ")";
                //     }
                // )
                circles.attr('cx', function (d) { return map.latLngToLayerPoint(d.latlng as any).x })
                    .attr('cy', function (d) { return map.latLngToLayerPoint(d.latlng as any).y });
            };
    
            this.map.on('viewreset', update);
            this.map.on('zoom', update);
            // this.map.on('moveend', update);
            update();
            
            // Tooltip Service
            // console.log('adding tooltips')
            this.tooltipServiceWrapper.addTooltip(circleGroup.selectAll('.circle'),
                (tooltipEvent: TooltipEventArgs<DataPoint>) => this.getTooltipData(tooltipEvent.data),
                (tooltipEvent: TooltipEventArgs<DataPoint>) => tooltipEvent.data.selectionID
            );

            // cleanup drawn svgs on update. Is this needed since the g is deleted at the beginning of the function?
            circles.exit()
            .remove();
            
            
            // Todo: Persist base layer. Add settings to objects
            this.map.on('baselayerchange', (e: any) => {
                this.host.persistProperties({
                    merge: [<VisualObjectInstance>{
                        objectName: "baseMap",
                        properties: {
                            "layer": e.name
                        },
                        selector: undefined
                    }]
                })
                // console.log(e.name);
            });
        }
    }

    private static parseSettings(dataView: DataView): VisualSettings {
        console.log('Getting Visual Settings')
        return <VisualSettings>VisualSettings.parse(dataView);
    }

    /**
     * This function gets called for each of the objects defined in the capabilities files and allows you to select which of the
     * objects and properties you want to expose to the users in the property pane.
     *
     */
    public enumerateObjectInstances(options: EnumerateVisualObjectInstancesOptions): VisualObjectInstance[] | VisualObjectInstanceEnumerationObject {
        return VisualSettings.enumerateObjectInstances(this.settings || VisualSettings.getDefault(), options);
    }

    private getTooltipData(value: any): VisualTooltipDataItem[] {
        // console.log('tooltip value', value);
        return value.tooltipItems;
        // return [{
        //     displayName: 'value.category',
        //     value: 'value.category.toString()',
        //     // color: value.color
        // }];
    }

    private getViewModel(options: VisualUpdateOptions): ViewModel {
        // get data from Power BI
        let dataView = options.dataViews;

        let viewModel: ViewModel = {
            dataPoints: [],
            latMax: this.latInitial,
            latMin: this.latInitial,
            lngMax: this.lngInitial,
            lngMin: this.lngInitial,
            highlights: false
        };
        
        // if no data, return empty view model
        if (!dataView
            || !dataView[0]
            || !dataView[0].categorical
            || !dataView[0].categorical.categories
            || !dataView[0].categorical.categories[0].source
            || !dataView[0].categorical.values
            || !dataView[0].metadata)
            return viewModel;
            
        let view = dataView[0].categorical;
        let metadata = dataView[0].metadata;
        
        let categories = view.categories;
        let categoricalRoles = categories.map(c => Object.keys(c.source.roles)[0]);
        let categoryIndex = dataRoleHelper.getCategoryIndexOfRole(categories, 'category')
        // let categoryIndex = categoricalRoles.findIndex(role => role === "category");
        let category = categories[categoryIndex];
    
        let values = view.values;
        let highlights = values[0].highlights;
        let dataRoles = values.map(v => Object.keys(v.source.roles)[0]);
        // // get display names for tooltips
        // let categoryColumnName = metadata.columns.filter(c => c.roles['category'])[0].displayName;
        // let displayNames = values.map(v => v.source.displayName);
        // displayNames.unshift(categoryColumnName);

        let tooltip = metadata.columns.filter(c => c.roles['tooltips']);
        // console.log('tooltip', tooltip);
        // console.log('values', values);
        // console.log('roles from values', dataRoles);
        // console.log('displayNames', displayNames);

        let latIndex = dataRoles.findIndex(role => role == 'latitude');
        let lngIndex = dataRoles.findIndex(role => role == 'longitude');
        let wktIndex = dataRoles.findIndex(role => role == 'wkt');
        let legendIndex = dataRoleHelper.getCategoryIndexOfRole(categories, 'legend')
        // let legendIndex = categoricalRoles.findIndex(role => role === "legend");
        let pointScaleIndex = dataRoles.findIndex(role => role === "pointScale");
        let wktScaleIndex = dataRoleHelper.getMeasureIndexOfRole(view.values.grouped(), 'wktScale');
        console.log('wktScale', wktScaleIndex);
        // let wktScaleIndex = dataRoles.findIndex(role => role === "wktScale");

        // set dataPoints
        console.log('pushing data to viewModel')
        for ( let i=0, 
            len=Math.max(category.values.length, 
                values[0].values.length); i < len; i++ ) {
                
                    let toPush: DataPoint = {
                        category: <string>category.values[i],
                        selectionID: this.host.createSelectionIdBuilder()
                            .withCategory(category, i)
                            .createSelectionId(),
                        highlighted: highlights ? highlights[i] ? true : false : false,
                        color: this.host.colorPalette.getColor('default').value,
                        tooltipItems:[
                        {displayName: category.source.displayName, value: `${category.values[i]}`}, //casting did not work
                    ]
                }
                if ((latIndex != -1) && (lngIndex != -1)) {
                    let lat = <number>values[latIndex].values[i];
                    let lng = <number>values[lngIndex].values[i];
                    toPush.lat = lat;
                    toPush.lng = lng;
                    toPush.latlng = new L.LatLng(lat,lng)
                    
                    let latDisplayName = <string>values[latIndex].source.displayName;
                    let lngDisplayName = <string>values[lngIndex].source.displayName;
                    toPush.tooltipItems.push({displayName: latDisplayName, value: (<number>values[latIndex].values[i]).toFixed(6)});
                    toPush.tooltipItems.push({displayName: lngDisplayName, value: (<number>values[lngIndex].values[i]).toFixed(6)});
                }
                
                if (wktIndex !== -1) {
                    toPush.wkt = <string>values[wktIndex].values[i];
                };
                
                if (legendIndex !== -1) {
                    let legend = <string>categories[legendIndex].values[i];
                    toPush.legend = legend;
                    let legendDisplayName = categories[legendIndex].source.displayName;
                    toPush.color = this.host.colorPalette.getColor(legend).value;
                    toPush.tooltipItems.push({displayName: legendDisplayName, value: legend});
                }
                
                if (pointScaleIndex !== -1) {
                    toPush.pointScale = <number>values[pointScaleIndex].values[i];
                    let pointScaleDisplayName = values[pointScaleIndex].source.displayName;
                    toPush.tooltipItems.push({displayName: pointScaleDisplayName, value: `${values[pointScaleIndex].values[i]}` })
                }
                if (wktScaleIndex !== -1) {
                    toPush.wktScale = <number>values[wktScaleIndex].values[i];
                    let wktScaleDisplayName = values[wktScaleIndex].source.displayName;
                    toPush.tooltipItems.push({displayName: wktScaleDisplayName, value: `${values[wktScaleIndex].values[i]}` })
                }
                
                viewModel.dataPoints.push(toPush);
        }
        // set data lat/lng boundaries
        viewModel.latMax = d3.max(viewModel.dataPoints, d => d.lat);
        viewModel.latMin = d3.min(viewModel.dataPoints, d => d.lat);
        viewModel.lngMax = d3.max(viewModel.dataPoints, d => d.lng);
        viewModel.lngMin = d3.min(viewModel.dataPoints, d => d.lng);

        // set scale min/max
        if (pointScaleIndex !== -1) {
            viewModel.pointScaleMax = d3.max(viewModel.dataPoints, d => d.pointScale);
            viewModel.pointScaleMin = d3.min(viewModel.dataPoints, d => d.pointScale);
        }
        if (wktScaleIndex !== -1) {
            viewModel.wktScaleMax = d3.max(viewModel.dataPoints, d => d.wktScale);
            viewModel.wktScaleMin = d3.min(viewModel.dataPoints, d => d.wktScale);
        };

        // set highlights
        viewModel.highlights = viewModel.dataPoints.filter(d => d.highlighted).length > 0;

        // console.log(viewModel);
        console.log('returning view model...');
        return viewModel;
    }

    private addTooltips( ) {

    }

    private addInteraction( ){

    }

    private callOsmPois(lat, lng, host, map, svg, pathcreator) {
        const Http = new XMLHttpRequest();
        let query = `http://overpass-api.de/api/interpreter?data=%2F*%0AThis%20has%20been%20generated%20by%20the%20overpass-turbo%20wizard.%0AThe%20original%20search%20was%3A%0A%E2%80%9Camenity%3Dbus_station%E2%80%9D%0A*%2F%0A%5Bout%3Ajson%5D%5Btimeout%3A25%5D%3B%0A%2F%2F%20gather%20results%0A%28%0A%20%20%2F%2F%20query%20part%20for%3A%20%E2%80%9Camenity%3Dbus_station%E2%80%9D%0A%20%20node%28around%3A10000%2C${lat}%2C${lng}%29%5B%22amenity%22%3D%22bus_station%22%5D%3B%0A%20%20way%28around%3A10000%2C${lat}%2C${lng}%29%5B%22amenity%22%3D%22bus_station%22%5D%3B%0A%20%20relation%28around%3A10000%2C${lat}%2C${lng}%29%5B%22amenity%22%3D%22bus_station%22%5D%3B%0A%29%3B%0A%2F%2F%20print%20results%0Aout%20body%3B%0A%3E%3B%0Aout%20skel%20qt%3B`;
        function loadDoc() {
            var response;
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    response = this.response;
                    let pois = osmtogeojson(JSON.parse(response));
                    console.log(pois);
                    let poiGroup = svg.append('g');
                    let poiPaths = poiGroup.selectAll('path')
                        .data(pois)
                        .join('path')
                        .classed('pois', true)
                        .style('pointer-events', 'auto');
                    poiPaths.attr('d', pathcreator);
                    L.geoJSON(pois as any, {
                        pointToLayer: function(geoJsonPoint, latlng) {
                            return L.circleMarker(latlng, {radius: 2, color: 'grey'})
                        },
                        onEachFeature: function(feature, layer) {
                            layer.bindPopup(feature.properties.name);
                        }
                    }).addTo(map);
                }
            };
            xhttp.open("GET", query, true);
            xhttp.send();
            return response
        };
        let response = loadDoc();
        // host.launchUrl(query);
        console.log(query)
        return response
    }
}