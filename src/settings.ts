/*
 *  Power BI Visualizations
 *
 *  Copyright (c) Microsoft Corporation
 *  All rights reserved.
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the ""Software""), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

"use strict";

import { dataViewObjectsParser } from "powerbi-visuals-utils-dataviewutils";
import DataViewObjectsParser = dataViewObjectsParser.DataViewObjectsParser;

export class VisualSettings extends DataViewObjectsParser {
      public dataPoint: dataPointSettings = new dataPointSettings();
      public baseMap: baseMapSettings = new baseMapSettings();
      }

    export class baseMapSettings {
      // Base map layer
      public layer: string = "";
    }
    
    export class dataPointSettings {
      // Fill color for the points when no legend is applied
      public pointFill: string = '';
      // Point size scaling
      public pointSizeScaling: number = 1;
      // Opacity for point fill
      public pointFillOpacity: number = 0.7
      // Color of point stroke
      public pointStrokeThickness: number = 2;
      // Point Stroke Color
      public pointStrokeColor: string = '';
      // Point Stroke Opacity
      public pointStrokeOpacity: number = 1;
      // Polygon Fill Opacity
      public wktFillOpacity: number = 0.5;
      // Polygon Stroke Thickness
      public wktStrokeThickness: number = 2;
      // Polygon Stroke Color
      public wktStrokeColor: string = "black";
      // Polygon Stroke Opacity
      public wktStrokeOpacity: number = 1;
      // Polygon static fill
      public wktFill: string = "";
     // Starting fill color for the wkt geometries when no legend or saturation is applied
      public wktFillGradientStart: string = "yellow";
     // Mid fill color for the wkt geometries when no legend or saturation is applied
     public wktSwitchFillGradientMid: boolean = false;
     public wktFillGradientMid: string = "yellow";
     // End fill color for the wkt geometries when no legend or saturation is applied
     public wktFillGradientEnd: string = "red";
     }

